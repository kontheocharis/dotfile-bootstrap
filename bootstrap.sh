#!/bin/sh

cd $HOME

if ! [ -x "$(command -v yay)" ]; then
    cd /tmp

    if [ -d yay ]; then
        git clone https://aur.archlinux.org/yay.git
    fi

    cd yay
    makepkg -si --noconfirm
    cd $HOME
fi

yay -S --noconfirm --needed vcsh git myrepos

if ! [-d "$HOME/.config/mr"]; then
    vcsh clone https://gitlab.com/kontheocharis-dotfiles/mr.git
fi

cd $HOME/.config/mr/config.d
for i in "$@"; do
    ln -s ../available.d/$i.vcsh $i.vcsh
done
cd $HOME
mr up

# create symlinks
if [ -d ".symlinks" ]; then
    cd .symlinks
    $FILES=$(find ./* -type f | cut -c2-)
    for i in $FILES; do
        BASENAME=$(basename $i)
        DIRNAME=$(dirname $i)
        mkdir -p "$DIRNAME"
        cd "$DIRNAME"

        if [ -f "$BASENAME" ]; then
            sudo mv "$BASENAME" "$BASENAME.dotbackup"
        fi

        sudo ln -s "$HOME/.symlinks$i" "$BASENAME"
    done
    cd $HOME
fi

# install packages
cd $HOME
if [ -d ".packages" ]; then
    for i in "$@"; do
        if [ -f ".packages/$i" ]; then
            set +e
            cat ".packages/$i" | yay -S --noconfirm --needed -
            set -e
        fi
    done
fi

# run init scripts
if [ -d ".init" ]; then
    for i in "$@"; do
        if [ -f ".init/$i" ]; then
            sh ".init/$i"
        fi
    done
fi
